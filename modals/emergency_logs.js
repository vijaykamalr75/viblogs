var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var vibEmergencySchema = new Schema({
  logid: {
    type: Schema.Types.ObjectId,
    index: true
  },
  emergency : {
    type : Number //0-no-emergency,1-emergency
  },
  checkout_by : {
    type: Number, // 1-user,2-admin
  },
  emergency_status : {
    type : Number //1-approved, 0-not approved
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  updated_date: {
    type: Date
  }
});
var updateDate = function(next) {
  this._update.updated_date = new Date();
  next();
};

vibEmergencySchema
  .pre("findOneAndUpdate", updateDate)
  .pre("findByIdAndUpdate", updateDate)
  .pre("update", updateDate)
  .pre("updateOne", updateDate);

var VibEmergencySchema = mongoose.model("vib_emergency_logs", vibEmergencySchema, "vib_emergency_logs");
module.exports = VibEmergencySchema;
