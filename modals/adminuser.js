var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var adminUserSchema = new Schema({
  username: {
    type: String,
    index: true
  },
  password: {
    type: String
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  updated_date: {
    type: Date
  }
});
var updateDate = function(next) {
  this._update.updated_date = new Date();
  next();
};

adminUserSchema
  .pre("findOneAndUpdate", updateDate)
  .pre("findByIdAndUpdate", updateDate)
  .pre("update", updateDate)
  .pre("updateOne", updateDate);

  var AdminUserSchema = mongoose.model("vib_admin",adminUserSchema,"vib_admin");
  module.exports = AdminUserSchema;
