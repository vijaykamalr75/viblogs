var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var vibLogsSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    index: true
  },
  device_id: {
    type: String
  },
  checkin: {
    type: Date
  },
  checkout: {
    type: Date
  },
  emergency: {
    type: Number, //0-no-emergency,1-emergency
    default: 0
  },
  emergency_time : {
    type: Date,
    default : function() {
      if(this.emergency){
        return Date.now();
      }
      return null;
    }
  },
  checkout_by: {
    type: Number // 1-user,2-admin
  },
  emergency_status: {
    type: Number, //1-approved, 0-not approved
    default : function() {
      if(this.emergency){
        return 0;
      }
      return 0 ;
    }
  },
  lat: {
    type: Number
  },
  lang: {
    type: Number
  },
  created_date: {
    type: Date,
    default:  ()=> new Date().toLocaleString()
  },
  updated_date: {
    type: Date
  }
});
var updateDate = function(next) {
  this._update.updated_date = new Date().toLocaleString();
  next();
};

vibLogsSchema
  .pre("findOneAndUpdate", updateDate)
  .pre("findByIdAndUpdate", updateDate)
  .pre("update", updateDate)
  .pre("updateOne", updateDate);

var VibLogsSchema = mongoose.model("vib_logs", vibLogsSchema, "vib_logs");
module.exports = VibLogsSchema;
