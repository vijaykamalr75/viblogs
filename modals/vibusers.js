var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var vibUserSchema = new Schema({
  name: {
    type: String,
    trim: true
  },
  employee_id: {
    type: String,
    index: true,
    trim: true
  },
  email_id: {
    type: String,
    index: true,
    trim: true
  },
  password: {
    type: String,
    trim: true
  },
  pan_no: String,
  experienced: Boolean,
  designation: String,
  experience_details: [
    {
      company_name: String,
      designation: String,
      location: String,
      experience: String
    }
  ],
  permanent_address: {
    street: String,
    state: String,
    city: String,
    pincode: String
  },
  residential_address: {
    street: String,
    state: String,
    city: String,
    pincode: String
  },
  status: {
    type: Number,
    default: 1
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  updated_date: {
    type: Date
  }
});
var updateDate = function(next) {
  this._update.updated_date = new Date();
  next();
};

vibUserSchema
  .pre("findOneAndUpdate", updateDate)
  .pre("findByIdAndUpdate", updateDate)
  .pre("update", updateDate)
  .pre("updateOne", updateDate);

var VibUserSchema = mongoose.model("vib_users", vibUserSchema, "vib_users");
module.exports = VibUserSchema;
