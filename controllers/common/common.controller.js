const CryptoJS = require("crypto-js");
const constant = require("../../config/constants");
var bcrypt = require("bcryptjs");
var nodemailer = require("nodemailer");
var generator = require("generate-password");
const config = require("../../config/env/" + process.env.NODE_ENV);
let commonFunctions = {
  responseReturn: responseReturn,
  getRequest: getRequest,
  createPassword: createPassword,
  checkPassword: checkPassword,
  sendEmail: sendEmail,
  randompassword: randompassword
};

function responseReturn(code, data = {}, status = false) {
  var result = {};
  result.code = code;
  result.message = getMessageByCode(code);
  if(data == null){
    data = {};
  }
  result.data =
    Object.getOwnPropertyNames(data).length === 0
      ? {}
      : CryptoJS.AES.encrypt(
          JSON.stringify(data),
          constant.PASS_PHRASE
        ).toString();
  result.success = status;
  return result;
}
function getMessageByCode(code) {
  var message = "Unknown error";

  //Check for the integer value
  //Check code must not be 0
  //check the lenght of the code value it 7 digits (including -)
  if (
    constant.codes[code] !== undefined &&
    Number.isInteger(code) &&
    code !== 0 &&
    code.toString().length <= 7
  ) {
    message = constant.codes[code];
  }
  return message;
}

function getRequest(getData) {
  //
  //
  console.log(getData);
  var bytes = CryptoJS.AES.decrypt(getData.data, constant.PASS_PHRASE);
  return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
}

function createPassword() {
  return new Promise(function(resolve, reject) {
    let password = randompassword();
    bcrypt.hash(password, 12, function(err, hash) {
      let passObj = {
        hash: hash,
        password: password
      };
      resolve(passObj);
    });
  });
}
function checkPassword(password, hashpassword) {
  return new Promise(function(resolve, reject) {
    bcrypt.compare(password, hashpassword).then(res => {
      resolve(res);
    });
  });
}
function randompassword() {
  var password = generator.generate({
    length: 10,
    numbers: true,
    strict: true
  });
  return password;
}
function sendEmail(username, password, emailid) {
  return new Promise(function(resolve, reject) {
    // create reusable transporter object using the default SMTP transport
    let smtpConfig = {
      host: config.mailoptions.host,
      port: config.mailoptions.port,
      secure: false, // upgrade later with STARTTLS
      auth: {
        user: config.mailoptions.auth_id,
        pass: config.mailoptions.auth_pass,
      }
    };
    let transporter = nodemailer.createTransport(smtpConfig);

    // setup email data with unicode symbols
    let mailOptions = {
      from: config.mailoptions.from, // sender address
      to: emailid, // list of receivers
      subject: "Password for Vibrant info", // Subject line
      text: "Hi " + username, // plain text body
      html: "<p>Your password is <b>" + password + "</b> </p>" // html body
    };
    // verify connection configuration
    transporter.verify(function(error, success) {
      if (error) {
        reject(error);
      } else {
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject(error);
          } else {
            resolve(info);
          }
        });
      }
    });
  });
}
module.exports = commonFunctions;
