const async = require("async");
const logsModal = require("../../modals/viblogs");
const gen = require("../common/common.controller");
const constant = require("../../config/constants");
const config = require("../../config/env/" + process.env.NODE_ENV);
var moment = require("moment");

let logFunctions = {
  userCheckin: userCheckin,
  userCheckout: userCheckout,
  alertEmergency: alertEmergency
};
function parseDate(date){
  let datestring = date.split('/');
  let datefinal = datestring[1]+"/"+datestring[0]+"/"+datestring[2];
  return datefinal;
}
function userCheckin(data, done) {
  try {
    let userid = data.id;
    let checkin = data.checkin;
    let latitude = data.lat;
    let longitude = data.lang;
    let deviceid = data.deviceid;
    var today = moment().startOf("day");
    var tomorrow = moment(today).endOf("day");
    let checkin_local = new Date(checkin).toLocaleString();
    checkin_local = moment(checkin_local).format();
    logsModal
      .findOne({
        user_id: userid,
        checkin: {
          $gte: today.toDate(),
          $lt: tomorrow.toDate()
        }
      })
      .exec((err, logresult) => {
        if (err)
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        if (logresult == null) {
          let logsmodal = new logsModal();
          logsmodal.user_id = userid;
          logsmodal.checkin = checkin_local;
          logsmodal.device_id = deviceid;
          logsmodal.lat = latitude;
          logsmodal.lang = longitude;
          logsmodal.save((err,saveresult) => {
            if(err){
             
            return done(
              null,
              gen.responseReturn(constant.CHECK_IN_FAIL, saveresult, true)
            ); 
            }
            return done(
              null,
              gen.responseReturn(constant.CHECK_IN, saveresult, true)
            );
          });
        } else {
          return done(
            null,
            gen.responseReturn(constant.DUP_CHECKIN, {}, false)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, error, false));
  }
}
function userCheckout(data, done) {
  try {
    let userid = data.id;
    let checkout = data.checkout;
    var today = moment().startOf("day");
    var tomorrow = moment(today).endOf("day");

    logsModal
      .findOne({
        user_id: userid,
        checkin: {
          $gte: today.toDate(),
          $lt: tomorrow.toDate()
        }
      })
      .exec((err, logresult) => {
        if (err)
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        if (logresult != null && Object.keys(logresult).length > 0) {
          if (logresult.checkout) {
            return done(
              null,
              gen.responseReturn(constant.DUP_CHECKOUT, {}, false)
            );
          } else {
            let momentcheckin = moment(logresult.checkin);
            let checkout_local = new Date(checkout).toLocaleString();
            let momentcheckout = moment(checkout_local);
            let timedifference =
              moment.duration(momentcheckin.diff(momentcheckout)).asHours() *
              -1;
            // if (timedifference < 4) {
            //   return done(
            //     null,
            //     gen.responseReturn(constant.MIN_CHECKOUT, {}, false)
            //   );
            // } else {
              let updatecheckout = {
                checkout: checkout,
                checkout_by: 1
              };
              logsModal
                .findOneAndUpdate(
                  {
                    user_id: userid,
                    checkin: {
                      $gte: today.toDate(),
                      $lt: tomorrow.toDate()
                    }
                  },
                  { $set: updatecheckout }
                )
                .exec((err, logresult) => {
                  if (err)
                    return done(
                      null,
                      gen.responseReturn(constant.EXCEPTION, {}, false)
                    );
                  else {
                    return done(
                      null,
                      gen.responseReturn(constant.CHECKED_OUT, logresult, true)
                    );
                  }
                });
            }
        //  } 
        } else {
          return done(
            null,
            gen.responseReturn(constant.NOTCHECK_IN, {}, false)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function alertEmergency(data, done) {
  try {
    let userid = data.id;
    var today = moment().startOf("day");
    var tomorrow = moment(today).endOf("day");
    logsModal
      .findOne({
        user_id: userid,
        checkin: {
          $gte: today.toDate(),
          $lt: tomorrow.toDate()
        }
      })
      .exec((err, logresult) => {
        if (err)
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        if (logresult != null && Object.keys(logresult).length > 0) {
          if (logresult.checkout) {
            return done(
              null,
              gen.responseReturn(constant.DUP_CHECKOUT, {}, false)
            );
          } else if (logresult.emergency) {
            return done(
              null,
              gen.responseReturn(constant.DUP_EMERGENCY, {}, false)
            );
          } else {  
            let emergencylog = {
              emergency : 1,
              emergency_time : new Date().toLocaleString(),
              emergency_status : 0
            }     
             
              logsModal
                .findOneAndUpdate(
                  {
                    user_id: userid,
                    checkin: {
                      $gte: today.toDate(),
                      $lt: tomorrow.toDate()
                    }
                  },
                  { $set: emergencylog }
                )
                .exec((err, logresult) => {
                  if (err)
                    return done(
                      null,
                      gen.responseReturn(constant.EXCEPTION, {}, false)
                    );
                  else {
                    return done(
                      null,
                      gen.responseReturn(constant.EMERGENCY_SUCCESS, logresult, true)
                    );
                  }
                });
            
          }
        } else {
          return done(
            null,
            gen.responseReturn(constant.NOTCHECK_IN, {}, false)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
module.exports = logFunctions;
