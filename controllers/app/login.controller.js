const async = require("async");
const usersModal = require("../../modals/vibusers");
const gen = require("../common/common.controller");
const constant = require("../../config/constants");
const config = require("../../config/env/" + process.env.NODE_ENV);
var jwt = require("jsonwebtoken");

let userFunctions = {
  userLogin: userLogin,
  userLogout: userLogout
};
function userLogout(data, header, done) {
  try {
    var userid = data.id;
    var accesstoken = header["x-access-token"];
    jwt.verify(accesstoken, config.USER_JWT_KEY, function(err, users) {
      if (err)  return done(null,gen.responseReturn(constant.EXCEPTION, {}, false));
      else {
        usersModal
          .findById(users.id, { password: 0 })
          .exec((err, userresult) => {
            if (err)
              return done(
                null,
                gen.responseReturn(constant.EXCEPTION, {}, false)
              );
            if (!userresult) {
              return done(null,gen.responseReturn(constant.NO_USER, {}, false));
            } else {
              const token = jwt.sign({ id: userresult._id }, config.jwt_key, {
                expiresIn: config.tokenLife
              });
              return done(
                null,
                gen.responseReturn(constant.SUCCESS, {}, true)
              );
            }
          });
      }
    });
  } catch (error) {
    console.log(error);
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function userLogin(data, done) {
  try {
    usersModal
      .findOne({ employee_id: data.employee_id, status: 1 })
      .exec((err, userresult) => {
        if (err)
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        if (userresult && Object.keys(userresult).length > 0) {
          let checkpassword = gen.checkPassword(
            data.password,
            userresult.password
          );
          checkpassword.then(function(result) {
            if (result) {
              let userobj = {
                name: userresult.name,
                id: userresult._id,
                employee_id: userresult.employee_id,
                email_id: userresult.email_id,
                token: jwt.sign({ id: userresult._id }, config.USER_JWT_KEY, {
                  expiresIn: 86400 // expires in 24 hours
                })
              };
              console.log(userobj);
              return done(
                null,
                gen.responseReturn(constant.LOGIN_SUCCESS, userobj, true)
              );
            } else {
              return done(
                null,
                gen.responseReturn(constant.LOGIN_ERROR, {}, false)
              );
            }
          });
        } else {
          return done(
            null,
            gen.responseReturn(constant.LOGIN_ERROR, {}, false)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
module.exports = userFunctions;
