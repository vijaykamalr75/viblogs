var async = require("async");
var userModal = require("../../modals/vibusers");
const gen = require("../common/common.controller");
const constant = require("../../config/constants");
let userFunctions = {
  addUser: addUser,
  editUser: editUser,
  deleteUser: deleteUser,
  listUser: listUser,
  getUser: getUser
};
function listUser(data, done) {
  try {
    userModal
      .find()
      .sort({ created_date: -1 })
      .exec((err, userlist) => {
        if (err) {
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        } else {
          return done(
            null,
            gen.responseReturn(constant.SUCCESS, userlist, true)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function getUser(data, done) {
  try {
    let userid = data.id;
    console.log(userid);
    userModal.findById(userid).exec((err, userresult) => {
      if (err)
        return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
      else {
        console.log(userresult);
        return done(
          null,
          gen.responseReturn(constant.SUCCESS, userresult, true)
        );
      }
    });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function deleteUser(data, done) {
  try {
    let status = data.status;
    let userid = data.id;
    userModal.findById(userid).exec((err, userresult) => {
      if (err)
        return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
      if (userresult && Object.keys(userresult).length > 0) {
        userModal
          .findByIdAndUpdate(userid, { $set: { status: status } })
          .exec((err, updateresult) => {
            if (err)
              return done(
                null,
                gen.responseReturn(constant.EXCEPTION, {}, false)
              );
            else
              return done(
                null,
                gen.responseReturn(
                  constant.UPDATE_USER_SUCCESS,
                  updateresult,
                  true
                )
              );
          });
      } else {
        return done(null, gen.responseReturn(constant.NO_USER, {}, false));
      }
    });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function editUser(data, done) {
  try {
    let userid = data.id;
    let name = data.name;
    let employee_id = data.emp_id;
    let email_id = data.email;
    let pan_no = data.pan_no;
    let designation = data.designation;

    let residential_address = {};
    let permanent_address = {};
    residential_address.street = data.residential_address.street || "";
    residential_address.city = data.residential_address.city || "";
    residential_address.state = data.residential_address.state || "";
    residential_address.pincode = data.residential_address.zip || "";
    if (data.same_address) {
      permanent_address = residential_address;
    } else {
      permanent_address.street = data.permanent_address.street || "";
      permanent_address.city = data.permanent_address.city || "";
      permanent_address.state = data.permanent_address.state || "";
      permanent_address.pincode = data.permanent_address.zip || "";
    }
    let experience_details = [];
    let experienced = data.experienced == "1" ? true : false;
    async.eachSeries(
      data.experience_details,
      (exp, expCall) => {
        let exp_obj = {};
        exp_obj.company_name = exp.company_name;
        exp_obj.designation = exp.designation;
        exp_obj.location = exp.location;
        exp_obj.experience = exp.experience;
        experience_details.push(exp_obj);
        expCall();
      },
      (err, results) => {
        userModal.findById(userid).exec((err, userresult) => {
          if (err)
            return done(
              null,
              gen.responseReturn(constant.EXCEPTION, {}, false)
            );
          if (userresult && Object.keys(userresult).length > 0) {
            userModal
              .findOne({
                $and: [
                  {
                    $or: [{ employee_id: employee_id }, { email_id: email_id }]
                  },
                  {
                    _id: {
                      $ne: userid
                    }
                  }
                ]
              })
              .exec((err, userlist) => {
                if (err)
                  return done(
                    null,
                    gen.responseReturn(constant.EXCEPTION, {}, false)
                  );
                if (userlist != null && userlist.employee_id == employee_id) {
                  return done(
                    null,
                    gen.responseReturn(constant.DUP_EMP_ID, {}, false)
                  );
                } else if (userlist != null && userlist.email_id == email_id) {
                  return done(
                    null,
                    gen.responseReturn(constant.DUP_EMAIL_ID, {}, false)
                  );
                } else {
                  let edituserObj = {
                    name: name,
                    employee_id: employee_id,
                    email_id: email_id,
                    pan_no: pan_no,
                    experienced: experienced,
                    designation: designation,
                    experience_details: experience_details,
                    permanent_address: permanent_address,
                    residential_address: residential_address
                  };
                  userModal
                    .findByIdAndUpdate(userid, { $set: edituserObj })
                    .exec((err, updateresult) => {
                      if (err)
                        return done(
                          null,
                          gen.responseReturn(constant.EXCEPTION, {}, false)
                        );
                      else
                        return done(
                          null,
                          gen.responseReturn(
                            constant.UPDATE_USER_SUCCESS,
                            updateresult,
                            true
                          )
                        );
                    });
                }
              });
          } else {
            return done(null, gen.responseReturn(constant.NO_USER, {}, false));
          }
        });
      }
    );
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}

function addUser(data, done) {
  try {
    let name = data.name;
    let employee_id = data.emp_id;
    let email_id = data.email;
    let pan_no = data.pan_no;
    let designation = data.designation;

    let residential_address = {};
    let permanent_address = {};
    residential_address.street = data.residential_address.street || "";
    residential_address.city = data.residential_address.city || "";
    residential_address.state = data.residential_address.state || "";
    residential_address.pincode = data.residential_address.zip || "";
    if (data.same_address) {
      permanent_address = residential_address;
    } else {
      permanent_address.street = data.permanent_address.street || "";
      permanent_address.city = data.permanent_address.city || "";
      permanent_address.state = data.permanent_address.state || "";
      permanent_address.pincode = data.permanent_address.zip || "";
    }
    let experience_details = [];
    let experienced = data.experienced == "1" ? true : false;
    async.eachSeries(
      data.experience_details,
      (exp, expCall) => {
        let exp_obj = {};
        exp_obj.company_name = exp.company_name;
        exp_obj.designation = exp.designation;
        exp_obj.location = exp.location;
        exp_obj.experience = exp.experience;
        experience_details.push(exp_obj);
        expCall();
      },
      (err, results) => {
        userModal
          .findOne({
            $or: [{ employee_id: employee_id }, { email_id: email_id }]
          })
          .exec((err, userresult) => {
            if (err)
              return done(
                null,
                gen.responseReturn(constant.EXCEPTION, {}, false)
              );
            if (userresult && Object.keys(userresult).length > 0) {
              if (userresult.employee_id == employee_id) {
                return done(
                  null,
                  gen.responseReturn(constant.DUP_EMP_ID, {}, false)
                );
              } else {
                return done(
                  null,
                  gen.responseReturn(constant.DUP_EMAIL_ID, {}, false)
                );
              }
            } else {
              let encryptpassword = gen.createPassword();
              encryptpassword.then(resultpass => {
                let users = new userModal();
                users.name = name;
                users.employee_id = employee_id;
                users.email_id = email_id;
                users.pan_no = pan_no;
                users.experienced = experienced;
                users.designation = designation;
                users.experience_details = experience_details;
                users.permanent_address = permanent_address;
                users.residential_address = residential_address;
                users.password = resultpass.hash;
                users.save((err, saveresult) => {
                  let sendmail = gen.sendEmail(
                    name,
                    resultpass.password,
                    email_id
                  );
                  sendmail.then(sentresult => {});
                  return done(
                    null,
                    gen.responseReturn(constant.USER_ADD, saveresult, true)
                  );
                });
              });
            }
          });
      }
    );
  } catch (error) {
    console.log(error);
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}

module.exports = userFunctions;
