var async = require("async");
var userModal = require("../../modals/vibusers");
var logsModal = require("../../modals/viblogs");
const gen = require("../common/common.controller");
const constant = require("../../config/constants");
var mongoose = require("mongoose");
var moment = require("moment");
let logFunctions = {
  loglist: loglist,
  userLogList: userLogList,
  adminCheckout: adminEmergencyCheckout,
  getEmergencyList: getEmergencyList
};
function getEmergencyList(data, done) {
  try {
    logsModal
      .find({
        emergency: 1,
        checkout_by: { $exists: false }
      })
      .exec((err, results) => {
        if (err) {
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        } else {
          return done(
            null,
            gen.responseReturn(constant.SUCCESS, results, true)
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function adminEmergencyCheckout(data, done) {
  try {
    let logid = data.id;
    let emergency_status = data.status;
    let checkout_local = new Date().toLocaleString();
    checkout_local = moment(checkout_local).format();
    logsModal.findById(logid).exec((err, results) => {
      console.log(err);
      if (err)
        return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
      else if (results) {
        if (results.emergency) {
          logsModal
            .findOneAndUpdate(
              { _id: logid },
              {
                $set: {
                  checkout: checkout_local,
                  emergency_status: emergency_status,
                  checkout_by: 2
                }
              }
            )
            .exec((err, updates) => {
              if (err) {
                return done(
                  null,
                  gen.responseReturn(constant.EXCEPTION, {}, false)
                );
              } else {
                return done(
                  null,
                  gen.responseReturn(constant.SUCCESS, updates, true)
                );
              }
            });
        } else {
          return done(
            null,
            gen.responseReturn(constant.EMERGENCY_NOT_FOUND, {}, false)
          );
        }
      } else {
        return done(null, gen.responseReturn(constant.NO_LOG, {}, false));
      }
    });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function loglist(data, done) {
  try {
    let getdate = data.date;
    var today = new Date(getdate);
    var tomorrow = new Date(
      new Date(getdate).setDate(new Date(getdate).getDate() + 1)
    );
    userModal
      .aggregate([
        {
          $lookup: {
            from: "vib_logs",
            localField: "_id",
            foreignField: "user_id",
            as: "logs"
          }
        },

        {
          $addFields: {
            logs: {
              $arrayElemAt: [
                {
                  $filter: {
                    input: "$logs",
                    as: "log",
                    cond: {
                      $and: [
                        {
                          $lte: ["$$log.checkin", tomorrow]
                        },
                        {
                          $gte: ["$$log.checkin", today]
                        }
                      ]
                    }
                  }
                },
                0
              ]
            }
          }
        }
      ])
      .exec((err, userlist) => {
        if (err) {
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        } else {
          var logarray = [];
          async.eachSeries(
            userlist,
            function(userlogs, logCall) {
              if (userlogs.logs) {
                logarray.push(userlogs);
              }
              logCall();
            },
            function(err, result) {
              console.log(logarray);
              return done(
                null,
                gen.responseReturn(constant.SUCCESS, logarray, true)
              );
            }
          );
        }
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
function userLogList(data, done) {
  try {
    userModal
      .aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(data.id)
          }
        },
        {
          $lookup: {
            from: "vib_logs",
            localField: "_id",
            foreignField: "user_id",
            as: "logs"
          }
        }
      ])
      .exec((err, logresult) => {
        if (err)
          return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
        else
          return done(
            null,
            gen.responseReturn(constant.SUCCESS, logresult, true)
          );
      });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}
module.exports = logFunctions;
