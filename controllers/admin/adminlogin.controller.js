var async = require("async");
var adminModal = require("../../modals/adminuser");
const gen = require("../common/common.controller");
const constant = require("../../config/constants");
const config = require("../../config/env/" + process.env.NODE_ENV);
var jwt = require("jsonwebtoken");
let adminlogFunctions = {
  adminLogin: adminLogin
};


function adminLogin(data, done) {
  try {
    adminModal.findOne({ username: data.username }).exec((err, userresult) => {
      if (err)
        return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
      if (userresult && Object.keys(userresult).length > 0) {
        let checkpassword = gen.checkPassword(
          data.password,
          userresult.password
        );
        checkpassword.then(function(result) {
          if (result) {
            let userobj = {
              username: userresult.username,
              id: userresult._id,
              token: jwt.sign({ id: userresult._id }, config.USER_JWT_KEY, {
                expiresIn: 86400 // expires in 24 hours
              })
            };
            return done(
              null,
              gen.responseReturn(constant.LOGIN_SUCCESS, userobj, true)
            );
          } else {
            return done(
              null,
              gen.responseReturn(constant.LOGIN_ERROR, {}, false)
            );
          }
        });
      } else {
        return done(null, gen.responseReturn(constant.LOGIN_ERROR, {}, false));
      }
    });
  } catch (error) {
    return done(null, gen.responseReturn(constant.EXCEPTION, {}, false));
  }
}

module.exports = adminlogFunctions;
