const express = require("express");
const router = express.Router();
var loginController = require("../controllers/app/login.controller");
var logscontroller = require("../controllers/app/logs.controller");

/**
 * Login Auth for app users
 * method - post
 */
router.post("/login", function(req, res, next) {
  loginController.userLogin(req.body, (err, result) => {
    res.json(result);
  });
});
/**
 * checkin for users
 * method - post
 */
router.post("/checkin", function(req, res, next) {
  logscontroller.userCheckin(req.body, (err, result) => {
    res.json(result);
  });
});
/**
 * checkout for users
 * method - post
 */
router.post("/checkout", function(req, res, next) {
  logscontroller.userCheckout(req.body, (err, result) => {
    res.json(result);
  });
});
/**
 * Emergency alert for app users
 * method - post
 */
router.post("/emergency", function(req, res, next) {
  logscontroller.alertEmergency(req.body, (err, result) => {
    res.json(result);
  });
});
/**
 * User logout
 * method - get
 */
router.get("/logout", function(req, res, next) {
  loginController.userLogout(req.body,req.headers, (err, result) => {
    res.json(result);
  });
});
module.exports = router;
