const express = require('express')
const router = express.Router();
const adminLoginController = require("../controllers/admin/adminlogin.controller");
const userController = require("../controllers/admin/users.controller");
const logsController = require("../controllers/admin/logs.controller");

/**
 * Login Auth for admin
 * method - post
 */
router.post('/login', function(req, res, next) {
    adminLoginController.adminLogin(req.body,(err,results)=>{
        res.json(results);
    })
 
});

/**
 * Function for add users
 * method - post
 */
router.post('/addUser', function(req, res, next) {
    userController.addUser(req.body,(err,results)=>{
        res.json(results);
    }) 
});
/**
 * Function for add users
 * method - post
 */
router.post('/getuser', function(req, res, next) {
    userController.getUser(req.body,(err,results)=>{
        res.json(results);
    }) 
})

/**
 * Function for edit users
 * method - post
 */
router.post('/editUser', function(req, res, next) {
    userController.editUser(req.body,(err,results)=>{
        res.json(results);
    }) 
})

/**
 * Function for deleteUser
 * method - post
 */
router.post('/deleteUser', function(req, res, next) {
    userController.deleteUser(req.body,(err,results)=>{
        res.json(results);
    }) 
})

/**
 * Function for get userslist
 * method - post
 */
router.post('/userlist', function(req, res, next) {
    userController.listUser(req.body,(err,results)=>{
        res.json(results);
    }) 
})
/**
 * Function for get logslist
 * method - post
 */
router.post('/loglist', function(req, res, next) {
    logsController.loglist(req.body,(err,results)=>{
        res.json(results);
    }) 
});
/**
 * Function for get user logslist
 * method - get
 */
router.post('/user', function(req, res, next) {
    logsController.userLogList(req.body,(err,results)=>{
        res.json(results);
    }) 
});
/**
 * Function for get user logslist
 * method - get
 */
router.post('/checkout', function(req, res, next) {
    logsController.adminCheckout(req.body,(err,results)=>{
        res.json(results);
    }) 
});
/**
 * Function for get user emergencylist
 * method - get
 */
router.get('/emergency_list', function(req, res, next) {
    logsController.getEmergencyList(req.body,(err,results)=>{
        res.json(results);
    }) 
})



module.exports = router
