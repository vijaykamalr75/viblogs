const express = require("express");
const helmet = require("helmet");
const path = require("path");
const http = require("http");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
var mongoose = require("mongoose");
const config = require("./config/env/" + process.env.NODE_ENV + ".js");
const constants = require("./config/constants");
const gen = require("./controllers/common/common.controller");
var adminModal = require("./modals/adminuser");
var userModal = require("./modals/vibusers");
var jwt = require("jsonwebtoken");
const mongooptions = {
  useNewUrlParser: true,
  autoReconnect: true,
  useCreateIndex: true
};
mongoose
  .connect(
    config.db_url,
    mongooptions
  )
  .then(
    () => {
      console.log("done");
    },
    err => {
      console.log(err);
    }
  );
const admin = require("./routes/admin");
const users = require("./routes/users");

const app = express();
var server = http.createServer(app);
app.disable("x-powered-by");
/* app.use(helmet.frameguard({ action: 'sameorigin' })); */
app.use(helmet.xssFilter());
app.use(helmet.noCache());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
    res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
 
  // Request headers you wish to allow
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,x-access-token"
  );
 
  // res.header("Access-Control-Allow-Headers", "*");
  next();
});
if (process.env.NODE_ENV != "development") {
  app.use(function(req, res, next) {
    try {
      if(req.method != 'GET')
       req.body = gen.getRequest(req.body);
      next();
    } catch (error) {
      next();
    }
  });

  app.use((req, res, next) => {
    try {
      const excludedAdminUrl = ["login"];
      let requrl = req.originalUrl.split("/");
      if (requrl[4].indexOf(excludedAdminUrl) >= 0) {
        next();
      } else if(1==2) {
        let reqtoken = req.headers["x-access-token"];
        if (!reqtoken) {
          res.json(gen.responseReturn(constants.JWTTOKEN_ERROR), {}, false);
        } else {
          jwt.verify(reqtoken, config.USER_JWT_KEY, function(err, users) {
            console.log(users);
            if (err)
              res.json(gen.responseReturn(constants.TOKEN_FAIL), {}, false);
            else {
              if (requrl[3] == "admin") {
                adminModal
                  .findById(users.id, { password: 0 })
                  .exec((err, userresult) => {
                    if (err)
                      res.json(
                        gen.responseReturn(constants.EXCEPTION),
                        {},
                        false
                      );
                    if (!userresult) {
                      res.json(
                        gen.responseReturn(constants.NO_USER),
                        {},
                        false
                      );
                    } else {
                      next();
                    }
                  });
              } else if (requrl[3] == "users") {
                userModal
                  .findById(users.id, { password: 0 })
                  .exec((err, userresult) => {
                    if (err)
                      res.json(
                        gen.responseReturn(constants.EXCEPTION),
                        {},
                        false
                      );
                    if (!userresult) {
                       next();
                      // res.json(
                      //   gen.responseReturn(constants.NO_USER),
                      //   {},
                      //   false
                      // );
                    } else {
                      next();
                    }
                  });
              } else {
                res.json(
                  gen.responseReturn(constants.INVALID_REQUEST),
                  {},
                  false
                );
              }
            }
          });
        }
      }else{
        next();
      }
    } catch (error) {
      res.json(gen.responseReturn(constants.EXCEPTION), {}, false);
    }
  });
}

app.use("/api/v1/admin", admin);
app.use("/api/v1/users", users);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  res.json(gen.responseReturn(constants.NOT_FOUND), {}, false);
  //next(err);
});
// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  // render the error page
  let errdata = err.message + "- date = " + new Date() + ";";
  res.json(gen.responseReturn(constants.INTERNAL_ERROR), {}, false);
  // return next(errdata);
});

app.listen(8082, function() {
  console.log("port listen on 8082");
});

module.exports = app;
